require 'rails_helper'

describe User do
  describe "Instance Methods" do

    let(:user) { FactoryBot.create(:user) }

    it "can generate token" do
      expect(user.token).to eq(nil)
      user.generate_token
      expect(user.token).not_to eq(nil)
    end

  end
end