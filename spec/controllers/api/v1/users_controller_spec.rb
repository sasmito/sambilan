require 'rails_helper'
require 'json'

describe Api::V1::UsersController do

  describe "authentication" do
    let(:email) { "sasmito@sambilan.com" }
    let(:password) { "password" }
    
    it "can login using email and password" do
      user = FactoryBot.create(:user, 
        email: email, 
        password: password
        )

      params = { email: "sasmito@sambilan.com", password: "password" }

      post :sign_in, body: params, format: :json

      resp = JSON.parse(response.body)
      expect(resp["status"]).to eq('Ok')
    end
  end
end