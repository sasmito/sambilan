FactoryBot.define do
  factory :user do
    email "#{SecureRandom.hex(6)}@gmail.com"
    password SecureRandom.hex(10)
    token nil
  end
end