# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
    # SEED for user
    # User.delete_all
    # Category.delete_all
    # Job.delete_all
    # Admin.delete_all


    User.create!(email: "test3@example.com", password: "testlele", password_confirmation: "testlele", user_detail_attributes: { role: "employer", fullname: "Fauzi", phone: "092322323", address: "sleman", gender: "pria", company_attributes:{ name:"rumah makan padang", address: "Jl. Pogung Kidul No.39, Sinduadi, Mlati, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281" } } )

    Admin.create!(email: "admintest@example.com", password: "admin123")

    Category.create!(name: "Makanan")

    Job.create!(title: "Kasir",
            desc: "Aliquam luctus tortor odio, vestibulum fermentum nulla congue eu. Sed venenatis eget erat id posuere. ",
            capacity: 15,
            salary: "240rb",
            start_due: "2018-01-05 16:44:09",
            end_due: "2018-01-10 16:44:09",
            expire_due: "2018-01-04 16:44:09",
            company_id: 1,
            category_id: 1,
            status: "active",
            is_verified: true)