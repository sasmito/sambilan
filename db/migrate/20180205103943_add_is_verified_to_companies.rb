class AddIsVerifiedToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :is_verified, :boolean
  end
end
