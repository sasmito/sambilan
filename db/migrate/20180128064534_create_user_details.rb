class CreateUserDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :user_details do |t|
      t.string :fullname
      t.boolean :is_verified
      t.string :gender
      t.string :role
      t.string :phone
      t.string :avatar_url
      t.text :address
      t.integer :user_id
      
      t.timestamps
    end
  end
end
