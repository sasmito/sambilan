class CreateWishlists < ActiveRecord::Migration[5.1]
  def change
    create_table :wishlists do |t|
    	t.integer :user_detail_id
    	t.integer :job_id

    	t.timestamps
    end
  end
end
