class CreateUsersSkills < ActiveRecord::Migration[5.1]
  def change
    create_table :users_skills do |t|
    	t.integer :skill_id
    	t.integer :user_detail_id

    	t.timestamps
    end
  end
end
