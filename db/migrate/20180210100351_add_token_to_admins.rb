class AddTokenToAdmins < ActiveRecord::Migration[5.1]
  def change
    add_column :admins, :token, :string
    add_index :admins, :token
  end
end
