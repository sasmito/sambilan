class CreateInvitations < ActiveRecord::Migration[5.1]
  def change
    create_table :invitations do |t|
      t.string :status
      t.integer :job_id
      t.integer :user_detail_id

      t.timestamps
    end
  end
end
