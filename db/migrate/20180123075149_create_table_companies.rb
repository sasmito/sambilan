class CreateTableCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.integer :user_detail_id
      t.string :name
      t.text :address
      t.text :desc
      t.string :logo_url

      t.timestamps
    end
  end
end
