class CreateJobs < ActiveRecord::Migration[5.1]
  def change

    create_table :jobs do |t|
      t.string :title
      t.text :desc
      t.integer :capacity
      t.string :salary
      t.string :start_due
      t.string :end_due
      t.string :expire_due
      t.integer :company_id
      t.integer :category_id
      t.boolean :is_verified
      t.string :status
      
      t.timestamps
    end
  end
end
