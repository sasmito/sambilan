class CreateApplies < ActiveRecord::Migration[5.1]
  def change
    create_table :applies do |t|
      t.string :status
      t.integer :rating
      t.integer :user_detail_id
      t.integer :job_id

      t.timestamps
    end
  end
end
