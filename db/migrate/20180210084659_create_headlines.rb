class CreateHeadlines < ActiveRecord::Migration[5.1]
  def change
    create_table :headlines do |t|
      t.string :title
      t.string :image
      t.integer :admin_id

      t.timestamps
    end
  end
end
