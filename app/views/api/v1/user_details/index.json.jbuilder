if @user_details
    json.status :ok
    json.message "load list user_details of page #{@page} with limit #{@limit} data"
    json.data do
        json.array! @user_details do |user_detail|
            json.except! user_detail, :created_at, :updated_at
            json.user do
                json.email user_detail.user.email
            end
            if user_detail.role == "employer"
                json.company user_detail.company
            end
        end
    end
    json.current_page @user_details.current_page
    json.limit_page @user_details.limit_value
    json.total_page @user_details.total_pages
    json.total_user_details @all_userdetails.count
end