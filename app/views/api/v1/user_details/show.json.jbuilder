if @user_detail
    json.status :ok
    json.message "User detail loaded"
    json.data do 
        json.except! @user_detail, :created_at, :updated_at
        json.user do
            json.email @user_detail.user.email
        end
        if @user_detail.role == "employer"
            json.company @user_detail.company
        end
    end
end