if @applies
    json.status :ok
    json.message "load list applies of page #{@page} with limit #{@limit} data"
    json.data do
        json.array! @applies do |apply|
            json.except! apply
            json.user_detail apply.user_detail
            json.job apply.job
        end
    end
    json.current_page @applies.current_page
    json.limit_page @applies.limit_value
    json.total_page @applies.total_pages
    json.total_applies @all_applies.count
end