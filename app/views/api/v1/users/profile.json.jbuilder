json.status :ok
json.message "User profile loaded"
json.data do 
    json.email @current_user.email
    json.user_detail do
        json.except! @current_user.user_detail, :created_at, :updated_at
        if @current_user.user_detail.role == "employer"
            json.company @current_user.user_detail.company
        end
    end
end