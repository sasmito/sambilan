json.status @status ? :ok : "error"
if @status
  json.message "Sign in success"
  json.data do
    json.id @user.id
    json.email @user.email
    json.token @user.token
    json.user_detail @user_detail
    if @user_detail.role == "employer"
      json.company @user_detail.company
    end
  end
  else
    json.message "Failed to sign in"
  end