json.status @status ? :ok : "error"
unless @status
  json.message @user.errors
else
  json.message "Akun berhasil terdaftar"
end