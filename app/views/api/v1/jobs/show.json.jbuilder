if @job_detail
    json.status :ok
    json.message "Job detail loaded"
    json.data do
        json.except! @job_detail
        json.category do
            json.except! @job_detail.category, :created_at, :updated_at, :admin_id
        end
        json.company do
            json.except! @job_detail.company, :created_at, :updated_at
            json.user_detail do
                json.except! @job_detail.company.user_detail, :created_at, :updated_at
            end
        end
        json.count_apply @job_detail.applies.count
        json.count_invitation @job_detail.invitations.count
    end
end