if @jobs
    json.status :ok
    json.message "load created company jobs list of page #{@page} with #{@limit} data"
    json.data do
        json.array! @jobs do |job|
            json.except! job
            json.category do
                json.except! job.category, :created_at, :updated_at, :admin_id
            end
            json.company job.company
            json.count_apply job.applies.count
            json.count_invitation job.invitations.count
        end
    end
    json.current_page @jobs.current_page
    json.limit_page @jobs.limit_value
    json.total_page @jobs.total_pages
    json.total_jobs @all_jobs.count
end    