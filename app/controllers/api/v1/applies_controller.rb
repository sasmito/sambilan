class Api::V1::AppliesController < Api::V1::ApplicationController
    before_action :set_apply, only: [:show, :update, :destroy]
    before_action :check_access_token
    before_action :check_employer, except: [:index, :show, :create, :destroy]
    before_action :check_employee, except: [:show, :update, :requested]
    before_action :set_pagination, only: [:index, :requested]

    # yang bisa update dan delete hanya yang berassoc dengan item itu
    # yg bisa update hanya company yang membuat jobs itu
    # sedangkan delete itu hanya user yang berassoc/apply pada applied job tsb

    # def index
    #     @all_applies = Apply.all
    #     @applies = Apply.order(:created_at).page(@page).per(@limit)
    # end

    def index
        if params.has_key?(:status)
            @status = params[:status]
        end

        if @status == "done"
            @all_applies = Apply.where(user_detail_id: @current_user.user_detail.id, status: @status).all
            @applies = Apply.where(user_detail_id: @current_user.user_detail.id, status: "done").or(Apply.where(user_detail_id: @current_user.user_detail.id, status: "rejected")).order(:created_at).page(@page).per(@limit)
        elsif @status
            @all_applies = Apply.where(user_detail_id: @current_user.user_detail.id, status: @status).all
            @applies = Apply.where(user_detail_id: @current_user.user_detail.id, status: @status).order(:created_at).page(@page).per(@limit)
        else
            @all_applies = Apply.where(user_detail_id: @current_user.user_detail.id).all
            @applies = Apply.where(user_detail_id: @current_user.user_detail.id).order(:created_at).page(@page).per(@limit)
        end
    end
    
    def requested
        company_jobs = @current_user.user_detail.company.jobs.ids
        @all_applies = Apply.where(job_id: company_jobs).all
        @applies = Apply.where(job_id: company_jobs).order(created_at: :desc).page(@page).per(@limit)
    end

    def create
        @apply = Apply.new(job_id: params[:job_id], user_detail_id: @current_user.user_detail.id)

        if @apply.save
            render json: {
                status: :ok,
                message: "Job applied",
                data: @apply
            }.to_json
        else
            render json: {
                status: "error",
                message: @apply.errors.full_messages
            }.to_json
        end

    end

    def show
        @apply_detail = Apply.find(params[:id])
    end

    # PUT /applies/:id
    def update
        if @apply.update(apply_params)
            render json: {
                status: :ok,
                message: "Apply updated",
                data: @apply
            }.to_json
        else
            render json: {
                status: "error",
                message: @apply.errors.full_messages
            }.to_json
        end
    end

    
    # DELETE /applies/:id
    def destroy
        
        if @apply.destroy
            render json: {
                status: :ok,
                message: "Apply canceled",
                data: @apply
            }.to_json
        else
            render json: {
                status: "error",
                message: @apply.errors.full_messages
            }.to_json
        end
    end
    
    private
    
    def check_employee
      render json: {status: "error", message: "User must be an employee"} unless employee?
    end
    
    def check_employer
      render json: {status: "error", message: "User must be an employer"} unless employer?
    end

    def apply_params
        # whitelist params
        params.permit(:job_id, :status)
    end

    def set_apply
        @apply = Apply.find(params[:id])
    end
    
    def set_pagination
        if params.has_key?(:page)
            @page = params[:page]
        else 
            @page = 1
        end

        if params.has_key?(:limit)
            @limit = params[:limit]
        else 
            @limit = 5
        end
    end

end