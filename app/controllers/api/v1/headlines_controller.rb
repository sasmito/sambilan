class Api::V1::HeadlinesController < Api::V1::ApplicationController
  before_action :set_headline, only: [:show, :update, :destroy]
  before_action :check_access_token_admin, except: [:index, :show]

  # GET /headlines
  # GET /headlines.json
  def index
    @headlines = Headline.all
    render json: {
      status: :ok,
      message: "Headlines loaded",
      data: @headlines
  }.to_json
  end

  # POST /headlines
  # POST /headlines.json
  def create
    params = headline_params.merge({admin_id: @current_user.id})
    @headlines = Headline.new(params)

    if @headlines.save
        render json: {
                status: :ok,
                message: "Headline created",
                data: @headlines}.to_json
    else
      render json: {
                status: "error",
                message: "failed to save"}.to_json
    end
    
  end

  # GET /headlines/1
  # GET /headlines/1.json
  def show
    @headline_detail = Headline.find(params[:id])
    render json: {
            status: :ok,
            message: "show headline detail",
            data: @headline_detail
          }.to_json
        
  end

  # PATCH/PUT /headlines/1
  # PATCH/PUT /headlines/1.json
  def update
    params = headline_params.merge({admin_id: @current_user.id})
    if @headline.update(params)
      render json: {
                status: :ok,
                message: "Headline updated",
                data: @headline
            }.to_json
    else
      render json: {
        status: "error",
        massage: "update salah"
    }.to_json
    end
  end

  # DELETE /headlines/1
  # DELETE /headlines/1.json
  def destroy
    if @headline.destroy
      render json: {
        status: :ok,
        message: "headline destroyed",
        data: @headline
    }.to_json
    else
      render json: {
        status: "error",
        message: "destroy error, try again"
        }.to_json
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_headline
      @headline = Headline.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def headline_params
      params.require(:headline).permit(:title, :image)
    end
end
