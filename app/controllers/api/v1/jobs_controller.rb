class Api::V1::JobsController < Api::V1::ApplicationController
    before_action :set_job, only: [:show, :update, :verify_job, :destroy]
    before_action :check_access_token, except: [:index, :show, :verify_job, :job_around, :landing]
    before_action :check_access_token_admin, only: [:verify_job]
    before_action :check_employer, except: [:index, :show, :verify_job, :job_around, :landing]
    before_action :set_pagination, only: [:index, :landing]

    def index

        unless request.headers["token"].nil?
            check_access_token
            if employer?
                @current_company = Company.find_by(id: @current_user.user_detail.company.id)
                @all_jobs = Job.where(company_id: @current_company.id).all
                @jobs = Job.where(company_id: @current_company.id).order(:created_at).page(@page).per(@limit)
            else
                @all_jobs = Job.all
                @jobs = Job.order(:created_at).page(@page).per(@limit)
            end
        else
            @all_jobs = Job.all
            @jobs = Job.order(:created_at).page(@page).per(@limit)
        end

    end

    def landing
        
        unless request.headers["token"].nil?
            check_access_token
            if employer?
                @all_emplyees = UserDetail.where(role: "employee")
                @emplyees = UserDetail.where(role: "employee").page(@page).per(@limit)
            else
                @all_jobs = Job.all
                @jobs = Job.order(:created_at).page(@page).per(@limit)
            end
        else
            @all_jobs = Job.all
            @jobs = Job.order(:created_at).page(@page).per(@limit)
        end
        
        # json_response(@jobs)
    end



    def job_around
        distance = 20
        center_point = [params[:latitude], params[:longitude]]
        box = Geocoder::Calculations.bounding_box(center_point, distance)
        @near = Company.within_bounding_box(box)
        @jobs_around = Job.where(company_id: @near.ids)

        if @jobs_around
            render json: {
                status: :ok,
                message: "Nearest jobs loaded",
                data: @jobs_around
            }.to_json
        else
            render json: {
                message: "No nearest jobs"
            }.to_json
        end
        
        
    end
    
    # POST /jobs
    def create
        # company id nya adalah user employer
        params = job_params.merge({company_id: @current_user.user_detail.company.id})
        @jobs = Job.new(params)

        if @jobs.save
            render json: {
                status: :ok,
                message: "Job created",
                data: @jobs
            }.to_json
        else
            render json: {
                status: "error",
                message: @jobs.errors.full_messages
            }.to_json
        end

        # json_response(@jobs, :created)
    end

    # GET /jobs/:id
    def show
        @job_detail = Job.find(params[:id])
    end

    # PUT /jobs/:id
    def update
        unless params.has_key?(:is_verified)
            if @job.update(job_params)
                render json: {
                    status: :ok,
                    message: "Job updated",
                    data: @job
                }.to_json
            else
                render json: {
                    status: "error",
                    message: "Update failed"
                }.to_json
            end
        else
            render json: {
                status: "error",
                message: "User must be an admin"
            }.to_json
        end
    end
    
    # PUT /jobs/:id
    def verify_job
        if @job.update(job_params)
            render json: {
                status: :ok,
                message: "Job updated",
                data: @job
            }.to_json
        else
            render json: {
                status: "error",
                message: "Update failed"
            }.to_json
        end
    end

    
    # DELETE /jobs/:id
    def destroy
        @job.destroy
        head :no_content
    end
    
    private
    
    def check_employer
      render json: {status: "error", message: "User must be an employer"} unless employer?
    end

    def job_params
        # whitelist params
        params.permit(:title, :desc, :capacity, :salary, :is_verified, :status, :category_id, :start_due, :end_due, :expire_due)
        # :latitude, :longitude
    end

    def set_job
        @job = Job.find(params[:id])
    end

    def set_pagination
        if params.has_key?(:page)
            @page = params[:page]
        else 
            @page = 1
        end

        if params.has_key?(:limit)
            @limit = params[:limit]
        else 
            @limit = 5
        end
    end

end