class Api::V1::AdminsController < Api::V1::ApplicationController
    before_action :check_access_token_admin, only: [:sign_out]

    def sign_in
        @admin = Admin.find_by(email: params[:email])
        @status = if @admin && @admin.valid_password?(params[:password])
            @admin.generate_token
            render json: {
                status: :ok,
                message: "Admin signed in",
                data: @admin
            }.to_json
        else
            render json: {
                status: "error",
                message: "Failed to sign in"
            }.to_json
        end
    end

    def sign_out
        if @current_user
        @current_user.update(token: nil)
        render json: {
            status: :ok,
            message: "Token has been release",
            data: @current_user.token
        }.to_json
        else
        render json: {
            status: "error",
            message: "Admin not found"
        }.to_json
        end
    end

    private

    def admin_params
        params.permit(:email, :password)
        
    end

end