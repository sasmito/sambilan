class Api::V1::ApplicationController < ApplicationController
    skip_before_action :verify_authenticity_token

    helper_method :current_user
    helper_method :employer?
    helper_method :employee?

    def check_access_token
        # data = JSON.parse(request.body.read)
        @current_user = User.find_by(token: request.headers["token"]) rescue nil
        return true if @current_user
        render json: {
          status: "error",
          message: "Token cannot be null"
        }.to_json
    end

    def check_access_token_admin
      @current_user = Admin.find_by(token: request.headers["token"]) rescue nil
      return true if @current_user
        render json: {
          status: "error",
          message: "Token cannot be null or maybe you aren't an admin"
        }.to_json
    end

    def current_user
        @current_user
    end

    def employer?
      @current_user.role == "employer" rescue false
    end
    
    def employee?
      @current_user.role == "employee" rescue false
    end
    
end