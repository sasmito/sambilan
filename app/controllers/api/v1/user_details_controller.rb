class Api::V1::UserDetailsController < Api::V1::ApplicationController
    # khusus untuk admin yg handle user
    before_action :set_user_detail, only: [:show, :update]
    before_action :check_access_token_admin, except: [:index, :show]
    
    
    def index
        if params.has_key?(:page)
            @page = params[:page]
        else 
            @page = 1
        end

        if params.has_key?(:limit)
            @limit = params[:limit]
        else 
            @limit = 5
        end
        
        if params.has_key?(:role)
            @all_userdetails = UserDetail.where(role: params[:role]).all
            @user_details = UserDetail.where(role: params[:role]).order(:created_at).page(@page).per(@limit)
        else
            @all_userdetails = UserDetail.all
            @user_details = UserDetail.order(:created_at).page(@page).per(@limit)
        end
    end

    def show
        
    end

    def update

        # user_params = {user_attributes: {email: params[:email], password: params[:password]}}
        
        # if params[:role] == "employer"
        #     user_details_params[:user_attributes][:company_attributes] = { name: params[:company_name], address: params[:company_address]}
        # end
   
        # params = user_details_params.merge(user_params)
        # debugger
        if @user_detail.update!(user_details_params)
            render json: {
                status: :ok,
                message: "User detail updated",
                data: @user_detail
            }.to_json
        else
            render json: {
                status: "error",
                message: "Update failed"
            }.to_json
        end
    end
    
    private

    def user_details_params
        params.permit(:fullname, :phone, :address, :gender, :is_verified, :avatar_url, :role, user_attributes: [:email, :password], company_attributes: [:latitude, :longitude, :logo_url, :name => :company_name, :address => :company_address])
        # 
    end

    def set_user_detail
        @user_detail = UserDetail.find(params[:id])
    end
    
end