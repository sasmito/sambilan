class Api::V1::UsersController < Api::V1::ApplicationController
  before_action :set_user, only: [:show, :update, :destroy] 
  before_action :check_access_token, only: [:sign_out, :profile, :update_profile]

  def sign_in
    @user = User.find_by(email: params[:email])
    @status = if @user && @user.valid_password?(params[:password])
      @user_detail = @user.user_detail
      @user.generate_token
      true
    else
      false
    end
  end
  
  def sign_up
    user_params = {email: params[:email], password: params[:password]}
    user_detail_params = {user_detail_attributes: { role: params[:role], fullname: params[:fullname], phone: params[:phone], address: params[:address], gender: params[:gender] }}
    
    if params[:role] == "employer"
      user_detail_params[:user_detail_attributes][:company_attributes] = { name: params[:company_name], address: params[:company_address]}
    end

    params = user_params.merge(user_detail_params)
    @user = User.new(params)
    
    @status = if @user.save
      true
    else
      false
    end
  end

  # GET /users/profile
  def profile
    
  end
  
  # POST /users/update_profile
  def update_profile
    # debugger
    @user = User.find_by(id: @current_user.id)
    if @user.update(user_params)
        render json: {
            status: :ok,
            message: "User profile updated",
            data: @current_user,
            data_detail: @current_user.user_detail,
            data_company: @current_user.user_detail.company
        }.to_json
    else
        render json: {
            status: "error",
            message: "Update failed"
        }.to_json
    end
  end

  def sign_out
    if @current_user
      @current_user.update(token: nil)
      render json: {
        status: :ok,
        message: "Token has been release",
        data: @current_user.token
      }.to_json
    else
      render json: {
        status: "error",
        message: "User not found"
      }.to_json
    end
  end

  private

  def user_params
    params.permit(:email, :password, user_detail_attributes: [:id, :fullname, :phone, :address, :gender, :avatar_url, :role])
    
  end

  def set_user
    @user = User.find(params[:id])
  end
  
    
end