class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # include ActionController::RequestForgeryProtection
  include Response
  # include ExceptionHandler

  rescue_from ActiveRecord::RecordNotFound do |exception|
    json_response({ status: "Error", message: exception.message }, :not_found)
  end
end
