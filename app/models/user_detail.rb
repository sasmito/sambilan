class UserDetail < ApplicationRecord
    belongs_to :user

    # For employer
    has_one :company
    
    # For employee
    has_and_belongs_to_many :skills
    has_many :applies
    has_many :invitations

    accepts_nested_attributes_for :company
    accepts_nested_attributes_for :user

    validates :fullname, :address, :phone, :role, :gender, presence: true
	# validates_associated :user

    GENDER_OPTIONS = %w(pria wanita)
    validates :gender, :inclusion => {:in => GENDER_OPTIONS}
    
    ROLE_OPTIONS = %w(employee employer)
    validates :role, :inclusion => {:in => ROLE_OPTIONS}

    before_validation :default_values, :on => :create
    def default_values
        self.is_verified = false 
    end
    
end