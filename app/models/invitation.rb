class Invitation < ApplicationRecord
    belongs_to :job
    belongs_to :user_detail
    
    validates :status, presence: true

    STATUS_OPTIONS = %w(waiting accepted rejected)
    validates :status, :inclusion => {:in => STATUS_OPTIONS}

    before_validation :default_values, :on => :create
    def default_values
        self.status = 'waiting' # note self.status = 'P' if self.status.nil? might be safer (per @frontendbeauty)
    end
end