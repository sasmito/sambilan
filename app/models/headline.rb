class Headline < ApplicationRecord
    belongs_to :admin

    validates :image, presence: true
    validates_associated :admin       
end
