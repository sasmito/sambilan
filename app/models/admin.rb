class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
         has_many :categories
         has_many :headlines
  
  def generate_token
    self.update_column(:token, SecureRandom.hex(32))    
  end

end
