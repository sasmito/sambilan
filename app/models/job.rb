class Job < ApplicationRecord
    belongs_to :company
    belongs_to :category

    has_many :invitations
    has_many :applies
    has_many :wishlists
    has_many :ratings

    validates :title, presence: true
    validates :start_due, presence: true
    validates :end_due, presence: true
    validates :expire_due, presence: true
    validates_associated :company
    validates_associated :category
  
    STATUS_OPTIONS = %w(active expire not_yet running done)
    validates :status, :inclusion => {:in => STATUS_OPTIONS}

    before_validation :default_values, :on => :create
    def default_values
        self.status = "active"
        self.is_verified = false
    end
end