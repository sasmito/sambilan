class Skill < ApplicationRecord
	has_and_belongs_to_many :user_details

	validates :name, presence: true
end