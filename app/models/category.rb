class Category < ApplicationRecord
    # belongs_to :admin
    has_many :jobs

    validates :name, presence: true
    # validates_associated :admin    
end