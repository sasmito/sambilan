class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable
  
  # has_secure_password
  
  has_one :user_detail
  accepts_nested_attributes_for :user_detail


  def role
    user_detail.role
  end
  
  def generate_token
    self.update_column(:token, SecureRandom.hex(32))
  end
  
  # validates_associated :user_detail

  # attr_accessor :company_name
  # validates_presence_of :company_name, if: Proc.new { self.user_detail.role = "employer" }
  
  # after_create :save_employer
  # def save_employer
  #   Company.create(user_detail_id: self.user_detail.id, name: self.company_name) 
  # end
  
end
