class Apply < ApplicationRecord
    belongs_to :job
    belongs_to :user_detail
    
    validates :status, presence: true
    # validates :rating, presence: true
    validates_uniqueness_of :job_id, scope: :user_detail

    STATUS_OPTIONS = %w(waiting rejected accepted done)
    validates :status, :inclusion => {:in => STATUS_OPTIONS}

    before_validation :default_values, :on => :create
    def default_values
        self.status = 'waiting' # note self.status = 'P' if self.status.nil? might be safer (per @frontendbeauty)
    end
end