class Company < ApplicationRecord
    belongs_to :user_detail
    has_many :jobs

    validates :name, :address, presence: true

    geocoded_by :address               # can also be an IP address
    after_validation :geocode          # auto-fetch coordinates

    before_save :default_values
    def default_values
        self.is_verified ||= false 
    end

end