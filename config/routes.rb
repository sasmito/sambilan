Rails.application.routes.draw do
  devise_for :admins
  # devise_for :users

  namespace 'api' do
    namespace 'v1' do
      resources :headlines, defaults: {format: 'json'}
      resources :jobs, defaults: {format: 'json'} do
        collection do
          get 'job_around'
          put '/verify_job/:id', to: 'jobs#verify_job'
        end
      end
      resources :applies, defaults: {format: 'json'} do
        collection do
          get 'requested'
        end
      end
      resources :users, defaults: {format: 'json'} do
        collection do
          post 'sign_in'
          post 'sign_up'
          post 'sign_out'
          get 'profile'
          post 'update_profile'
        end
      end
      resources :admins, defaults: {format: 'json'} do
        collection do
          post 'sign_in'
          post 'sign_out'
        end
      end
      resources :user_details, defaults: {format: 'json'}
      get 'landing', to:'jobs#landing', defaults: {format: 'json'}

    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
